This repository contains the build scripts for a container image
that supports Fedora's *Porting To Modern C* effort:

* https://fedoraproject.org/wiki/Changes/PortingToModernC
* https://fedoraproject.org/wiki/Toolchain/PortingToModernC

To build the container image locally, run

   podman build .

in this directory.  A pre-built container image is available at the
Gitlab registry:

   registry.gitlab.com/fweimer-rh/fedora-modernc-container:modernc
