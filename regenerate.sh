#!/bin/bash

epoch=`date +%s`
iso=`date --utc +%Y-%m-%dT%H:%M:%SZ`
sed -i "s/^\\(RUN dnf -y update # \\).*/\\1$iso/" Containerfile
export GIT_AUTHOR_DATE="$epoch +0000"
export GIT_COMMITTER_DATE="$epoch +0000"
git commit -q -m "Regenerate ($iso)" Containerfile
